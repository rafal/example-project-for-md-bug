## Production

1. Follow [installation process](#installation) from step 1 to step 5 \(including step 5\)

2. Run

   ```
   ./install.sh
   ```

   Then fill the copied files \(settings\_local.py, loggers.py\) to meet production needs, database info etc.

3. Run

   ```bash
   ./run.sh
   ```

   If UWSGI process exists, the script will reload it, otherwise it will create a process for us.